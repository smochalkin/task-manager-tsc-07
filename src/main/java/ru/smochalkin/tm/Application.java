package ru.smochalkin.tm;

import ru.smochalkin.tm.constant.ArgumentConst;
import ru.smochalkin.tm.constant.TerminalConst;
import ru.smochalkin.tm.model.Command;
import ru.smochalkin.tm.repository.CommandRepository;
import ru.smochalkin.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        parseArgs(args);
        process();
    }

    public static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        exit();
    }

    public static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.HELP: showHelp(); break;
            case ArgumentConst.ABOUT: showAbout(); break;
            case ArgumentConst.VERSION: showVersion(); break;
            case ArgumentConst.INFO: showInfo(); break;
            default: displayErrorArgument();
        }
        exit();
    }

    public static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.HELP: showHelp(); break;
            case TerminalConst.ABOUT: showAbout(); break;
            case TerminalConst.VERSION: showVersion(); break;
            case TerminalConst.INFO: showInfo(); break;
            case TerminalConst.EXIT: exit(); break;
            default: displayErrorCommand();
        }
    }

    public static void process(){
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.print("Enter command: ");
            command = scanner.nextLine();
            parseCommand(command);
            System.out.println();
        }
    }

    public static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public static void displayErrorCommand() {
        System.err.println("Command not found...");
    }

    public static void displayErrorArgument() {
        System.err.println("Argument not supported...");
        System.exit(1);
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = CommandRepository.getCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Sergey Mochalkin");
        System.out.println("smochalkin@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    private static void showInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + processors);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void exit() {
        System.exit(0);
    }

}
